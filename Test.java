package learnJavaOne;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.stream.Collectors;

/**
 * 
 * @author whatt
 *
 *         1. Create a data structure (perhaps a List<Hashmap()>) or a List or Object that can store the above data
 *         - perhaps create a class called FlightPermission that has attributes Pilot (String),
 *         Aircraft (String), Aerodrome (String)?
 *         2. Write some code to create the list of data above
 *         3. Write a method to summarise the permissions where possible e.g.
 *         PilotA has permission to fly B-737 & B-738 to/from Adelaide (ADL)
 *
 * 
 *         The summaries should be natural english, so construct a list like you would in english, e.g.
 *         PilotB has permissions to fly a B-738 to/from Sydney (SYD)
 *         PilotB has permissions to fly a B-373 & B-738 to/from Sydney (SYD)
 *         PilotB has permissions to fly a B-732, B-373 & B-738 to/from Sydney (SYD)
 */

public class Test {

	private static final String BAD_DATA_NOTICE_MESSAGE = "Incomplete data is trying to be run:";
	private static final String BAD_DATA_EXIT_MESSAGE = "Records are incomplete, please sanitise data before reattempting.";
	private static final String ARRAY_OOB_ERROR_MESSAGE = "Array index out of bounds - consider data sanitization.";
	private static final String EMPTY_PERMISSIONS = "Permissions list is empty.";
	private static final String RETURN_NEW_LINE = "\r\n";
	private static final String TO_FROM = " to/from ";
	private static final String HAS_PERMISSIONS_TO_FLY_A = " has permissions to fly a ";
	private static final String NEW_LINE = "\n";

	public static final String PERMISSIONS_STRING = "PilotA has permissions to fly a B-737 to/from Adelaide (ADL)\r\n"
			+ "PilotA has permissions to fly a B-738 to/from Adelaide (ADL)\r\n"
			+ "PilotA has permissions to fly a B-738 to/from Brisbane (BNE)\r\n"
			+ "PilotA has permissions to fly a B-738 to/from Sydney (SYD)\r\n"
			+ "PilotA has permissions to fly a B-738 to/from Hobart (HOB)\r\n"
			+ "PilotA has permissions to fly a B-738 to/from Perth (PER)\r\n"
			+ "PilotB has permissions to fly a B-737 to/from Adelaide (ADL)\r\n"
			+ "PilotB has permissions to fly a B-738 to/from Adelaide (ADL)\r\n"
			+ "PilotB has permissions to fly a B-738 to/from Adelaide (ADL)\r\n"
			+ "PilotB has permissions to fly a B-757 to/from Adelaide (ADL)\r\n"
			+ "PilotB has permissions to fly a B-738 to/from Adelaide (ADL)\r\n"
			+ "PilotB has permissions to fly a B-738 to/from Adelaide (ADL)\r\n"
			+ "PilotB has permissions to fly a B-747 to/from Adelaide (ADL)\r\n"
			+ "PilotB has permissions to fly a B-737 to/from Brisbane (BNE)\r\n"
			+ "PilotB has permissions to fly a B-737 to/from Sydney (SYD)\r\n"
			+ "PilotB has permissions to fly a B-738 to/from Sydney (SYD)\r\n"
			+ "PilotC has permissions to fly a B-737 to/from Adelaide (ADL)\r\n"
			+ "PilotC has permissions to fly a B-738 to/from Adelaide (ADL)\r\n"
			+ "PilotC has permissions to fly a B-737 to/from Brisbane (BNE)\r\n"
			+ "PilotC has permissions to fly a B-737 to/from Sydney (SYD)\r\n"
			+ "PilotC has permissions to fly a B-738 to/from Sydney (SYD)\r\n"
			+ "PilotD has permissions to fly a B-737 to/from Adelaide (ADL)\r\n"
			+ "PilotD has permissions to fly a B-738 to/from Adelaide (ADL)\r\n"
			+ "PilotE has permissions to fly a B-738 to/from Brisbane (BNE)\r\n"
			+ "PilotE has permissions to fly a B-737 to/from Sydney (SYD)\r\n"
			+ "PilotE has permissions to fly a B-247 to/from Hobart (HOB)\r\n"
			+ "PilotE has permissions to fly a B-738 to/from Hobart (HOB)\r\n"
			+ "PilotD has permissions to fly a B-738 to/from Hobart (HOB)\r\n"
			+ "PilotB has permissions to fly a B-738 to/from Hobart (HOB)\r\n"
			+ "PilotB has permissions to fly a B-707 to/from Darwin (DAR)\r\n"
			+ "PilotC has permissions to fly a B-707 to/from Darwin (DAR)\r\n"
			+ "PilotE has permissions to fly a B-707 to/from Darwin (DAR)\r\n";

	// look at treemaps/hashmaps/sets
	// combo of maps to optimise

	public Test() {
	}

	public static void main(String[] args) throws Exception {
		System.out.println(PERMISSIONS_STRING + NEW_LINE);

		ArrayList<FlightPermission> permissionsList = createObjectList(PERMISSIONS_STRING);
		ArrayList<FlightPermission> groupedPermissions = groupPermissions(permissionsList);
		ArrayList<FlightPermission> sortedPermissions = sort(groupedPermissions);
		ArrayList<FlightPermission> formattedPermissions = format(sortedPermissions);

		for (FlightPermission fp2 : formattedPermissions) {
			System.out.println(fp2);
		}
		System.out.println(NEW_LINE);
	}

	public static ArrayList<FlightPermission> createObjectList(String permissions) throws Exception {
		ArrayList<FlightPermission> arrayList = new ArrayList<FlightPermission>();

		try {
			String csvPermissions = convertString(permissions);
			arrayList = createFlightPermissions(csvPermissions);
			arrayList = removeDuplicatePermissions(arrayList);
		} catch (NullPointerException e) {
			throw new NullPointerException(EMPTY_PERMISSIONS);
		}

		return arrayList;
	}

	public static String convertString(String permissions) {
		permissions = permissions.replace(HAS_PERMISSIONS_TO_FLY_A, ",");
		permissions = permissions.replace(TO_FROM, ",");
		permissions = permissions.replace(RETURN_NEW_LINE, ",");

		return permissions;
	}

	public static ArrayList<FlightPermission> createFlightPermissions(String csvPermissions) throws Exception {
		ArrayList<FlightPermission> flightPermissions = new ArrayList<FlightPermission>();
		try {
			ArrayList<String> csvList = new ArrayList<String>(Arrays.asList(csvPermissions.split(",")));
			flightPermissions = createFlightPermissions(csvList);
		} catch (IndexOutOfBoundsException e) {
			throw new IndexOutOfBoundsException(ARRAY_OOB_ERROR_MESSAGE);
		}
		return flightPermissions;
	}

	public static ArrayList<FlightPermission> createFlightPermissions(ArrayList<String> csvList) {
		ArrayList<FlightPermission> flightPermissions = new ArrayList<FlightPermission>();
		if (csvList.size() % 3 != 0) {
			System.out.println(BAD_DATA_EXIT_MESSAGE);
			System.exit(1);
		}
		for (int i = 0; i < csvList.size(); i = i + 3) {
			FlightPermission fp = new FlightPermission(csvList.get(i), csvList.get(i + 1), csvList.get(i + 2));

			// Assumptions
			// All Aerodromes names will contain (a-zA-Z{3})
			if (csvList.get(i + 2).matches(".+\\(\\S+\\)")) {
				flightPermissions.add(fp);
			} else {
				System.out.println(BAD_DATA_NOTICE_MESSAGE + NEW_LINE
						+ fp.toString() + NEW_LINE);
			}
		}

		return flightPermissions;
	}

	public static ArrayList<FlightPermission> removeDuplicatePermissions(ArrayList<FlightPermission> rawPermissions) {
		ArrayList<FlightPermission> uniquePermissions = (ArrayList<FlightPermission>) rawPermissions.stream()
				.distinct()
				.collect(Collectors.toList());

		return uniquePermissions;
	}

	public static ArrayList<FlightPermission> groupPermissions(ArrayList<FlightPermission> flightPermissions) {
		// Grouping by aircraft for each aerodrome
		ArrayList<FlightPermission> tempPermissions = flightPermissions;
		ArrayList<FlightPermission> permissionsToRemove = new ArrayList<FlightPermission>();

		for (FlightPermission fp : flightPermissions) {
			for (FlightPermission tp : tempPermissions) {
				if (fp != tp) {
					if (fp.getPilot().equals(tp.getPilot())
							&& fp.getAerodrome().equals(tp.getAerodrome())
							&& !tp.getAircraft().contains(fp.getAircraft())) {
						fp.setAircraft(fp.getAircraft().concat(", " + tp.getAircraft()));
						permissionsToRemove.add(tp);
					}
				}
			}
		}
		for (FlightPermission r : permissionsToRemove) {
			flightPermissions.remove(r);
		}

		// Grouping by aerodrome and aircraft for each pilot
		ArrayList<FlightPermission> tempPermissions2 = flightPermissions;
		ArrayList<FlightPermission> permissionsToRemove2 = new ArrayList<FlightPermission>();
		
		for (FlightPermission fp : flightPermissions) {
			for (FlightPermission tp : tempPermissions2) {
				if (fp != tp) {
					if (fp.getPilot().equals(tp.getPilot())
							&& fp.getAircraft().equals(tp.getAircraft())
							&& !fp.getAerodrome().equals(tp.getAerodrome())
							&& !permissionsToRemove2.contains(fp)) {
						fp.setAerodrome(fp.getAerodrome().concat(", " + tp.getAerodrome()));
						permissionsToRemove2.add(tp);
					}
				}
			}
		}
		for (FlightPermission r : permissionsToRemove2) {
			flightPermissions.remove(r);
		}

		// Grouping by pilot, aircraft and aerodrome
		ArrayList<FlightPermission> tempPermissions3 = flightPermissions;
		ArrayList<FlightPermission> permissionsToRemove3 = new ArrayList<FlightPermission>();

		for (FlightPermission fp : flightPermissions) {
			for (FlightPermission tp : tempPermissions3) {
				if (!fp.getPilot().equals(tp.getPilot())
						&& fp.getAircraft().equals(tp.getAircraft())
						&& fp.getAerodrome().equals(tp.getAerodrome())
						&& !permissionsToRemove3.contains(fp)) {
					if (fp.getPilot().compareTo(tp.getPilot()) > 0) {
						fp.setPilot(tp.getPilot().concat(", " + fp.getPilot()));
					} else {
						fp.setPilot(fp.getPilot().concat(", " + tp.getPilot()));
					}
					permissionsToRemove3.add(tp);
				}
			}
		}
		for (FlightPermission r : permissionsToRemove3) {
			flightPermissions.remove(r);
		}

		return flightPermissions;
	}

	public static ArrayList<FlightPermission> sort(ArrayList<FlightPermission> permissionsList) {
		Collections.sort(permissionsList, Comparator.comparing(FlightPermission::getPilot)
				.thenComparing(FlightPermission::getAerodrome));

		return permissionsList;
	}

	public static ArrayList<FlightPermission> format(ArrayList<FlightPermission> unformattedPermissions) {
		ArrayList<FlightPermission> formattedPermissions = new ArrayList<FlightPermission>();

		for (FlightPermission up : unformattedPermissions) {
			int i = up.getAircraft().lastIndexOf(",");
			if (i >= 0)
				up.setAircraft(new StringBuilder(up.getAircraft()).replace(i, i + 1, " &").toString());
			int j = up.getAerodrome().lastIndexOf(",");
			if (j >= 0)
				up.setAerodrome(new StringBuilder(up.getAerodrome()).replace(j, j + 1, " &").toString());
			int k = up.getPilot().lastIndexOf(",");
			if (k >= 0)
				up.setPilot(new StringBuilder(up.getPilot()).replace(k, k + 1, " &").toString());
			formattedPermissions.add(up);
		}

		return formattedPermissions;
	}
}
