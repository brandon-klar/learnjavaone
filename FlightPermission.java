package learnJavaOne;

public class FlightPermission {

	private String SINGLE_PILOT = "has";
	private String MULTIPLE_PILOTS = "have";
	private String pilot;
	private String aircraft;
	private String aerodrome;

	public String getPilot() {
		return pilot;
	}

	public void setPilot(String pilot) {
		this.pilot = pilot;
	}

	public String getAircraft() {
		return aircraft;
	}

	public void setAircraft(String aircraft) {
		this.aircraft = aircraft;
	}

	public String getAerodrome() {
		return aerodrome;
	}

	public void setAerodrome(String aerodrome) {
		this.aerodrome = aerodrome;
	}

	public FlightPermission() {

	}

	public FlightPermission(String pilot, String aircraft, String aerodrome) {
		this.pilot = pilot;
		this.aircraft = aircraft;
		this.aerodrome = aerodrome;
	}

	@Override
	public String toString() {
		String hasHave = "";
		if (!pilot.contains("&")) {
			hasHave = SINGLE_PILOT;
		} else {
			hasHave = MULTIPLE_PILOTS;
		}
		
		return pilot + " " + hasHave + " permissions to fly a " + aircraft + " to/from " + aerodrome;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == this) {
			return true;
		}

		if (!(obj instanceof FlightPermission)) {
			return false;
		}

		FlightPermission c = (FlightPermission) obj;

		return (pilot.equals(c.pilot) 
				&& aircraft.equals(c.aircraft)
				&& aerodrome.equals(c.aerodrome));
	}

	@Override
	public int hashCode() {
		return this.pilot.hashCode() + this.aircraft.hashCode() + this.aerodrome.hashCode();
	}
}
